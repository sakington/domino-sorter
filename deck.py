import random
from domino import *

class Deck():
    def __init__(self):
        #Builds a deck of dominoes by creating the objects
        self.poolOfDominoes = []

        #Creates domino objects 
        for c in reversed(range(0,10)): #Include the Highest Double ( 9 )
            for i in range(c,10):
                self.poolOfDominoes.append(Domino(i,c))

        #Shuffle list of dominoes
        random.shuffle(self.poolOfDominoes)

    #override to return the list as a string
    def __str__(self):
        deckString = ""
        
        for item in self.poolOfDominoes:
            deckString += "{0} : {1} \n".format(item.sideA,item.sideB)

        return deckString

    #---The following functions are for game functionality---

    #shuffle the deck
    def shuffle(self):
        random.shuffle(self.poolOfDominoes)

    #Deal a single peice
    def dealPeice(self):
	    return self.poolOfDominoes.pop(0)

    #Deal 7 peices to begin a game
    def dealSeven(self):
        returnHand = []
        for item in self.poolOfDominoes[0:7]:
            returnHand.append(item)

        del self.poolOfDominoes[0:7]
        return returnHand

    #Count the remaining dominoes 
    def count(self):
        return len(self.poolOfDominoes)
        
