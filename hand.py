import random
from domino import *

class Hand():
    def __init__(self,dealt):
        #Takes in hand that was dealt
        self.handOfDominoes = dealt

    #override to return the list as a string
    def __str__(self):
        handString = ""
        
        for item in self.handOfDominoes:
            handString += "{0} : {1} \n".format(item.sideA,item.sideB)

        return handString

    #---The following functions are for game functionality---

    #shuffle the hand
    def shuffle(self):
        random.shuffle(self.handOfDominoes)

    #play a peice from hand taking as a parameter the digits of both sides
    def playPeice(self,sideA,sideB):
        count = 0
        for item in self.handOfDominoes:
            if (item.sideA == sideA and item.sideB == sideB):
                return self.handOfDominoes.pop(count)
            count += 1
        return False

    #take a domino object and add it to the hand
    def drawPeice(self,domino):
        self.handOfDominoes.append(domino)

    #find the highest double within hand
    def highDouble(self):
        highDomino = Domino(None,None)
        for item in self.handOfDominoes:
            if (item.isDouble() == True and
                (highDomino.sideA == None or
                 item.totalValue() > highDomino.totalValue())):
                highDomino = item
        if (highDomino.sideA != None):
            return highDomino
        else:
            return False

    #Count the remaining dominoes 
    def count(self):
        return len(self.handOfDominoes)

    #Total the score of the dominoes in hand
    def total(self):
        score = 0
        for item in self.handOfDominoes:
            score += item.totalValue()

        return score
