from player import *
from deck import *
import copy
import random
import types

'''
The Object of this test is to try to break it by feeding it hands of dominoes
'''

while True:
	playerDeck = Deck()
	
	ComputerPlayer = Computer('test',1,playerDeck.dealSeven(),'Easy')
	
	print(ComputerPlayer)
	print('')
	for domino in ComputerPlayer.hand.handOfDominoes:
		print(domino)
	
	print('')
	
	#Find the dominoes that match the starting domino start paths with these (Defaults to 9)
	ComputerPlayer.buildRoot()
	print("Path Roots")
	for domino in ComputerPlayer.possibleMoves:
		print(domino)
	
	#Build possible paths of dominoes	
	ComputerPlayer.buildPath()
	print("Paths")
	for dominolist in ComputerPlayer.possibleMoves:
		for domino in dominolist:
			print(domino)
		print('')
		
	#Remove paths that are shorter than the longest path
	ComputerPlayer.getLongestPath()
	print("Longest Paths")
	
	try:	
		if type(ComputerPlayer.possibleMoves[0]) is list:
			for dominolist in ComputerPlayer.possibleMoves:
				for domino in dominolist:
					print(domino)
				print('')
		else:
			for domino in ComputerPlayer.possibleMoves:
				print(domino)
	except:
		for domino in ComputerPlayer.possibleMoves:
				print(domino)

	#Remove paths that have less double dominoes than the list with the most
	ComputerPlayer.getMostDoublesPath()
	print("Most Double Domino Paths")
	
	try:	
		if type(ComputerPlayer.possibleMoves[0]) is list:
			for dominolist in ComputerPlayer.possibleMoves:
				for domino in dominolist:
					print(domino)
				print('')
		else:
			for domino in ComputerPlayer.possibleMoves:
				print(domino)
	except:
		for domino in ComputerPlayer.possibleMoves:
				print(domino)

	#Remove paths that have more of the starting double than the path with the least
	#Default is 9, and as an example we are looking for the path with the least 9's
	ComputerPlayer.getPathLeastRoot()
	print("Least amount of the starting double")
	
	try:	
		if type(ComputerPlayer.possibleMoves[0]) is list:
			for dominolist in ComputerPlayer.possibleMoves:
				for domino in dominolist:
					print(domino)
				print('')
		else:
			for domino in ComputerPlayer.possibleMoves:
				print(domino)
	except:
		for domino in ComputerPlayer.possibleMoves:
				print(domino)
				
	#Find the path that gets rid of the most points the soonest
	ComputerPlayer.getPathHighScore()
	print("Path that removes the most points")
	
	try:	
		if type(ComputerPlayer.possibleMoves[0]) is list:
			for dominolist in ComputerPlayer.possibleMoves:
				for domino in dominolist:
					print(domino)
				print('')
		else:
			for domino in ComputerPlayer.possibleMoves:
				print(domino)
	except:
		for domino in ComputerPlayer.possibleMoves:
				print(domino)
				
	#Gather Dominoes that do not fit in best path
	ComputerPlayer.buildLists()
	
	print("Best Path Dominoes")
	for domino in ComputerPlayer.possibleMoves:
		print(domino)
	print("")
		
	print("Extra Dominoes in Hand")
	for domino in ComputerPlayer.extraDominoes:
		print(domino)	
	print('')
				
	input("Enter something: ")
