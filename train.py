from domino import *

#train that is specific to every player
class Train():
    #initialize a empty list, close the train, get current games double
    def __init__(self, default):
        self.dominoList = list()
        self.isOpen = False
        self.dominoList.append(default)

    #override to have the train print its contents
    def __str__(self):
        trainString = ""
        
        for item in self.dominoList:
            trainString += "{0} : {1} \n".format(item.sideA,item.sideB)

        return trainString

    #Toggle weither or not the train is open
    def toggleOpen(self):
        if self.isOpen == False:
            self.isOpen = True
            return True
        else:
            self.isOpen = False
            return False

    #determine if the passed domino matches the end
    #if train is empty match game double
    def isMatch(self, side):
        if self.dominoList[-1].sideB == side:
            return True
        else:
            return False

    #Apply domino to train
    def assignDomino(self, domino):
        if self.dominoList[-1].sideB == domino.sideA:
            self.dominoList.append(domino)
            return True
        elif self.dominoList[-1].sideB == domino.sideB:
            domino.rotate()
            self.dominoList.append(domino)
            return True
        else:
            return False

#common Train that is always open
class MexicanTrain(Train):
    #initialize a empty list, override isOpen, get current games double
    def __init__(self, default):
        Train.__init__(self, default)
        self.isOpen = True

    #override to just return open
    def toggleOpen(self):
        return True

