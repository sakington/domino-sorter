class Domino():
    def __init__(self, sideA, sideB):
        self.sideA = sideA
        self.sideB = sideB

    def __str__(self):
        return "{0} : {1}".format(self.sideA,self.sideB)

    def totalValue(self):
        number = self.sideA + self.sideB
        return int(number)

    def isDouble(self):
        return (self.sideA == self.sideB)

    def rotate(self):
        tempVar = self.sideA
        self.sideA = self.sideB
        self.sideB = tempVar
