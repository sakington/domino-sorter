from domino import *
from hand import *
from train import *
import copy
import random
import types

class Player():
    def __init__(self, name, position, dealtHand):
        self.name = str(name) # Player Name used to identify
        self.position = position # used to determine what turn you will go
        self.hand = Hand(dealtHand)# every player has a specific Hand
        self.train = Train(Domino(9,9)) # every player has a specific train
        self.score = 0 # score of user
        self.matchScore = 0 # score to be added
        self.lastPlayedDomino = Domino(9,9)
        
    def __str__(self):
        return "{0} is a Human Player".format(self.name)
    
class Computer(Player):
    def __init__(self, name, position, dealtHand, difficulty):
        Player.__init__(self,name,position,dealtHand)
        self.difficulty = difficulty # easy, or normal
        #self.builtList = list()
        self.extraDominoes = list() # dominoes not in best path
        self.possibleMoves = list()
        #self.pathMatch = False
        
    def __str__(self):
        return "{0} is a Computer Player".format(self.name)
    
#----------- NORMAL AI -----------------------------------------------------------

    '''
    Important Variables:

    possibleMoves: Container of Paths
    possiblePaths: Keeps count of how many paths a computer can take
    currentBranch: Place holder of which path is currently being worked on
    remainingDominoes: Keeps count of how many dominoes are in players hand (counts down when dominoes are tried and failed)
    branchMatched: Used to fork Paths
    currentPeice: Last domino in path used to determine matches
    
    '''
    def buildLists(self):
        #Clear out old data
        self.extraDominoes = list()
        self.possibleMoves = list()
        
        #Build Best Path
        self.buildRoot()
        self.buildPath()
        self.getLongestPath()
        self.getMostDoublesPath()
        self.getPathLeastRoot()
        self.getPathHighScore()

        #clean up possibleMoves into a list of dominoes
        try:
            if type(self.possibleMoves[0]) is list:
                self.possibleMoves = self.possibleMoves[0]
        except:
            self.extraDominoes = self.hand.handOfDominoes
            return False

        #Build Extras Path
        self.gatherLeftOverDominoes()
        
    #buildRoot goes through the players and finds possible matches
    def buildRoot(self):
        for item in self.hand.handOfDominoes:
            if self.train.isMatch(item.sideA) or self.train.isMatch(item.sideB):
                if self.train.isMatch(item.sideB):
                    item.rotate()
                moves = list()
                newItem = copy.deepcopy(item)
                moves.append(newItem)
                self.possibleMoves.append(moves)

    def buildPath(self):
        def ItemNotInList(item, currentPath):
            for currentPeice in currentPath:
                if (item.sideA == currentPeice.sideA and item.sideB == currentPeice.sideB) or\
                (item.sideA == currentPeice.sideB and item.sideB == currentPeice.sideA):
                    return False
            return True
    
        possiblePaths = len(self.possibleMoves) 
        currentBranch = 0 
        while possiblePaths != 0:
            possiblePaths = len(self.possibleMoves)-1
            remainingDominoes = len(self.hand.handOfDominoes)
            branchMatched = False      

            while remainingDominoes > 0:
                try:
                    currentPeice = self.possibleMoves[currentBranch][-1]
                except IndexError:
                    possiblePaths = 0
                    break

                for item in self.hand.handOfDominoes:
                    remainingDominoes -= 1
                    if item.sideA == currentPeice.sideB or item.sideB == currentPeice.sideB:
                        if ItemNotInList(item, self.possibleMoves[currentBranch]):
                            newItem = copy.deepcopy(item)
                            if item.sideB == currentPeice.sideB:
                                    newItem.rotate()
                                    
                            if branchMatched == False:                        
                                self.possibleMoves[currentBranch].append(newItem)
                                branchMatched = True
                                currentPeice = self.possibleMoves[currentBranch][-2]
                                remainingDominoes = len(self.hand.handOfDominoes)
                            else:
                                newBranch = copy.deepcopy(self.possibleMoves[currentBranch])
                                newBranch[-1] = newItem
                                self.possibleMoves.append(newBranch)
                                possiblePaths += 1
                                remainingDominoes = len(self.hand.handOfDominoes)
                else:
                    branchMatched = False

            else:
                currentBranch += 1
                continue

    def getLongestPath(self):
        if len(self.possibleMoves) > 1:
            if type(self.possibleMoves[0]) is list:
                #find the Longest path length in list
                longestLength = max(len(item) for item in self.possibleMoves)
                
                #Go through and delete any list thats not as long as the longest
                try:
                    for c in range(len(self.possibleMoves)-1,-1,-1):
                        if len(self.possibleMoves[c]) < longestLength:
                            del(self.possibleMoves[c])
                except:
                    None #I just want it to break when it iterates to the end of the list

        else:
            try:
                if type(self.possibleMoves[0]) is list:
                    self.possibleMoves = self.possibleMoves[0]
            except:
                return False

    def getMostDoublesPath(self):
        if len(self.possibleMoves) > 1:
            if type(self.possibleMoves[0]) is list():
                numberOfDoubles = list()

                #iterate down each path counting doubles 
                for itemList in self.possibleMoves:
                    tempCounter = 0
                    for domino in itemList:
                        if domino.isDouble == True:
                            tempCounter += 1
                    numberOfDoubles.append(tempCounter)

                #find the Longest path length in list
                mostDoubles = max(numberOfDoubles)
                
                #Go through and delete any list thats not as long as the longest
                try:
                    for c in range(0,len(numberOfDoubles)):
                        if self.possibleMoves[c] < mostDoubles:
                            del(self.possibleMoves[c])
                except:
                    None

        else:
            try:
                if type(self.possibleMoves[0]) is list:
                    self.possibleMoves = self.possibleMoves[0]
            except:
                return False

    def getPathLeastRoot(self):
        if len(self.possibleMoves) > 1:
            if type(self.possibleMoves[0]) is list:
                startNumber = self.train.dominoList[0].sideA
                numberOfStartNumber = list()

                #iterate down each path counting doubles 
                for itemList in self.possibleMoves:
                    tempCounter = 0
                    for domino in itemList:
                        if domino.sideA == startNumber or domino.sideB == startNumber:
                            tempCounter += 1
                    numberOfStartNumber.append(tempCounter)

                #find the Longest path length in list
                leastStartNumber = min(numberOfStartNumber)

                #Go through and delete any list thats not as long as the longest
                for c in range(len(numberOfStartNumber)-1,-1,-1):
                    if numberOfStartNumber[c] > leastStartNumber:
                        del(self.possibleMoves[c])
                
        else:
            try:
                if type(self.possibleMoves[0]) is list:
                    self.possibleMoves = self.possibleMoves[0]
            except:
                return False

    def getPathHighScore(self):
        if len(self.possibleMoves) > 1:
            if type(self.possibleMoves[0]) is list:
                pathLength = len(self.possibleMoves[0]) # Will lose 1 from its value until one path remains
                while True:
                    if len(self.possibleMoves) > 1:
                        pointsHalfPath = list()

                        #iterate down each path counting doubles 
                        for itemList in self.possibleMoves:
                            tempCounter = 0
                            for c in range(0,pathLength):
                                try:
                                    tempCounter += itemList[c].totalValue()
                                except:
                                    None
                            pointsHalfPath.append(tempCounter)

                        #find the Longest path length in list
                        mostPointsHalfWay = max(pointsHalfPath)

                        #Go through and delete any list thats not as long as the longest
                        try:
                            for c in range(len(pointsHalfPath)-1,-1,-1):
                                if pointsHalfPath[c] < mostPointsHalfWay:
                                    del(self.possibleMoves[c])
                        except:
                            None
                        if len(self.possibleMoves) > 1:
                            pathLength -= 1
                            pointsHalfPath = list()
                            
                    else:
                            pathLength -= 1
                            pointsHalfPath = list()
                            break

        else:
            try:
                if type(self.possibleMoves[0]) is list:
                    self.possibleMoves = self.possibleMoves[0]
            except:
                return False
            
    #Add Dominoes not in Best Path to extras list
    def gatherLeftOverDominoes(self):
        for handDomino in self.hand.handOfDominoes:
            if not len(self.possibleMoves)<1:
                for bestPathDomino in self.possibleMoves:
                    if ((handDomino.sideA == bestPathDomino.sideA and
                         handDomino.sideB == bestPathDomino.sideB) or
                        (handDomino.sideA == bestPathDomino.sideB and
                         handDomino.sideB == bestPathDomino.sideA)):
                        break
                else:
                    self.extraDominoes.append(handDomino)
            else:
                self.extraDominoes = self.hand.handOfDominoes
            

